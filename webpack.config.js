const path = require('path')

// Plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// Other
const dirname = path.resolve(path.dirname(''))
const outputPath = path.resolve(dirname, 'dist')
const isProd = process.env.NODE_ENV === 'production'

console.log(`Running in "${process.env.NODE_ENV}" mode`)

const config = {
  mode: process.env.NODE_ENV,
  entry: {
    main: './src/index.js'
  },
  output: {
    path: outputPath,
    filename: '[name].bundle.js',
    publicPath: '/'
  },
  devtool: isProd ? 'none' : 'source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: 'images'
          }
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: {
          loader: 'file-loader',
          options: {
            outputPath: 'fonts'
          }
        }
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'react-svg-loader',
            options: {
              jsx: true
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ['node_modules']
  },
  plugins: [
    new MiniCssExtractPlugin({ filename: '[name].css' }),
    new HtmlWebpackPlugin({ template: 'public/index.html', filename: 'index.html', hash: true }),
    new CopyWebpackPlugin([{ context: 'public', from: '**/*', to: outputPath, ignore: 'index.html' }])
  ],
  optimization: {
    minimizer: !isProd ? [] : [
      new UglifyJsPlugin({ parallel: true, sourceMap: !isProd }),
      new OptimizeCSSAssetsPlugin({})
    ],
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendor',
          chunks: 'initial'
        }
      }
    }
  }
}

if (!isProd) {
  config.devServer = {
    inline: true,
    port: 3000,
    compress: true,
    historyApiFallback: true, // 404 will fallback to index.html
    publicPath: '/', // webpack output
    contentBase: path.resolve(dirname, 'public') // content not from webpack
  }
}

module.exports = config
