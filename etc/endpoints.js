const ENDPOINTS = {
  LOGIN: '/login',
  LOGOUT: '/logout',
  CONNECT: '/connect',
  USERS: '/users',
  ARTICLES: {
    ROOT: '/articles',
    BY_UID: '/articles/:uid',
    RELATED: '/articles/related/:uid',
    STATEMENTS: '/articles/:uid/statements'
  },
  STATEMENTS: {
    ROOT: '/statements'
  }
}

export default ENDPOINTS
