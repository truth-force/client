import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {
  Button,
  Switch,
  FormGroup,
  InputGroup
} from '@blueprintjs/core'

import { ROUTES_DICT } from '../../routes'
import { authLogin } from '../../../store/actions/auth'
import stateChangeBuilder from '../../../utils/state-change-builder'

import './style.scss'

class LoginScene extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      remember: false,
      usernameHelper: null,
      passwordHelper: null
    }

    this.valueChange = stateChangeBuilder(this, (e) => e.target.value)
    this.checkedChange = stateChangeBuilder(this, (e) => e.target.checked)
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit (e) {
    const { username, password, remember } = this.state
    e.preventDefault()
    if (!username || !password) {
      this.setState({
        usernameHelper: username ? null : 'Required field',
        passwordHelper: password ? null : 'Required field'
      })
    } else {
      this.props.dispatch(authLogin(username, password, remember)).then(({ user }) => {
        this.setState({ usernameHelper: null, passwordHelper: null })
        if (user) {
          // show alert
        }
      })
    }
  }

  renderInputUsername (disabled) {
    const { username, usernameHelper } = this.state
    return <FormGroup helperText={usernameHelper} label='Username' labelFor='username-input'>
      <InputGroup type='text' id='username-input' value={username} onChange={this.valueChange('username')} leftIcon='person' disabled={disabled} />
    </FormGroup>
  }

  renderInputPassword (disabled) {
    const { password, passwordHelper } = this.state
    return <FormGroup helperText={passwordHelper} label='Password' labelFor='password-input'>
      <InputGroup type='password' id='password-input' value={password} onChange={this.valueChange('password')} leftIcon='lock' disabled={disabled} />
    </FormGroup>
  }

  renderInputRemember (disabled) {
    const { remember } = this.state
    return <Switch checked={remember} label='Remember Me' onChange={this.checkedChange('remember')} disabled={disabled} />
  }

  render () {
    const { isFetching } = this.props.store.auth
    return <div className='login-scene'>
      <div className='login-header'>
        <div className='left'><Button icon='home' onClick={() => { this.props.history.push(ROUTES_DICT.INDEX) }} /></div>
        <div className='right'><h2>Sign In</h2></div>
      </div>
      <div className='login-body'>
        <form onSubmit={this.onSubmit}>
          <div className='form-row'>{this.renderInputUsername(isFetching)}</div>
          <div className='form-row'>{this.renderInputPassword(isFetching)}</div>
          <div className='form-row'>{this.renderInputRemember(isFetching)}</div>
          <div className='form-submit'><Button loading={isFetching} type='submit'>Submit</Button></div>
        </form>
      </div>
      <div className='login-footer'>
        <span>Or create account <Link to={ROUTES_DICT.SIGNUP}>here</Link></span>
      </div>
    </div>
  }
}

export default connect((state) => {
  return {
    store: {
      auth: state.auth
    }
  }
})(LoginScene)
