import React from 'react'
import { Route } from 'react-router-dom'

import { ROUTES_DICT } from '../../../routes'

import ArticlePage from '../../../comopnents/article-page'

class WorkingPanel extends React.PureComponent {
  render () {
    return <Route path={ROUTES_DICT.ARTICLE_READ} component={ArticlePage} />
  }
}

export default WorkingPanel
