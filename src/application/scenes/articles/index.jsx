import React from 'react'
import { connect } from 'react-redux'
import { Scrollbars } from 'react-custom-scrollbars'

import WorkingPanel from './extends/working-panel'
import ArticlesList from '../../comopnents/articles-list'

import './style.scss'

class ArticlesScene extends React.PureComponent {
  renderContent () {
    return <div className='content'>
      <div className='articles'>
        <div className='title'>Online</div>
        <ArticlesList />
      </div>
    </div>
  }

  renderRightSide () {
    return <div className='working-panel'><WorkingPanel /></div>
  }

  render () {
    return <Scrollbars autoHide>
      <div className='articles-scene'>
        <div className='scene-wrapper'>
          <div className='left-side'>
            <div className='menu-panel'>
              menu this
            </div>
          </div>
          {this.renderContent()}
          {this.renderRightSide()}
        </div>
      </div>
    </Scrollbars>
  }
}

export default connect((state) => {
  return {
    store: {}
  }
})(ArticlesScene)
