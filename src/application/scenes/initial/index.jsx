import React from 'react'
import {
  Button
} from '@blueprintjs/core'

import { ROUTES_DICT } from '../../routes'

class InitialScene extends React.PureComponent {
  render () {
    return <div className='initial-scene'>
      <Button onClick={() => { this.props.history.push(ROUTES_DICT.LOGIN) }}>Go to login scene</Button>
      <Button onClick={() => { this.props.history.push(ROUTES_DICT.ARTICLES) }}>Go to articles</Button>
    </div>
  }
}

export default InitialScene
