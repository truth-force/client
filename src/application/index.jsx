import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Preloader from './preloader'
import generateRoutes from './routes'

import './style.scss'

class Application extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      ready: false
    }

    this.onReady = this.onReady.bind(this)
  }

  componentDidMount () {
    this.routes = generateRoutes()
  }

  onReady () {
    this.setState({ ready: true })
  }

  renderPrelaoder () {
    return <Preloader onReady={this.onReady} />
  }

  renderApplication () {
    if (!this.state.ready) {
      return null
    }
    return this.routes
  }

  render () {
    return <React.Fragment>
      {this.renderPrelaoder()}
      {this.renderApplication()}
    </React.Fragment>
  }
}

Application.propTypes = {
  services: PropTypes.object.isRequired
}

export default connect()(Application)
