import React from 'react'
import classNames from 'classnames'
import PropTypes from 'prop-types'

import ParagraphEditor from '../paragraph-editor'

import './style.scss'

class ArticleParagraphs extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      paragraphs: null,
      selected: null
    }

    this.doSelectParagraph = this.doSelectParagraph.bind(this)
    this.doUnselectParagraph = this.doUnselectParagraph.bind(this)
    this.doApplyStatement = this.doApplyStatement.bind(this)
  }

  componentDidMount () {
    const { text } = this.props
    this.setState({
      paragraphs: text.split('\n')
    })
  }

  doSelectParagraph (selected) {
    this.setState({ selected })
  }

  doUnselectParagraph () {
    this.setState({ selected: null })
  }

  doApplyStatement (statement) {
    statement.paragraph = this.state.selected
    return this.props.doApplyStatement(statement)
  }

  renderText (text, i) {
    if (this.state.selected !== null && this.state.selected === i) {
      return <ParagraphEditor text={text} doCancel={this.doUnselectParagraph} doApply={this.doApplyStatement} />
    }
    return <div onClick={() => this.doSelectParagraph(i)}>{text}</div>
  }

  renderParagraph (text, i) {
    const classes = classNames([
      'article-paragraph',
      {
        selected: this.state.selected !== null,
        active: this.state.selected === i
      }
    ])
    return <div key={i} className={classes}>{this.renderText(text, i)}</div>
  }

  render () {
    if (!this.state.paragraphs) {
      return null
    }
    return this.state.paragraphs.map((text, i) => this.renderParagraph(text, i))
  }
}

ArticleParagraphs.propTypes = {
  text: PropTypes.string.isRequired,
  doApplyStatement: PropTypes.func.isRequired
}

export default ArticleParagraphs
