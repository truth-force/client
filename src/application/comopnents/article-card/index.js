import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import {
  Button,
  ButtonGroup,
  Spinner
} from '@blueprintjs/core'

import { ROUTES_DICT } from '../../routes'
import { articleRelatedLoad } from '../../../store/actions/articles'

import './style.scss'

class ArticleCard extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      isFetchingRelated: false,
      isFetchingCurrent: false,
      isRelatedOpen: false
    }

    this.doSelectArticle = this.doSelectArticle.bind(this)
    this.doSearchRelated = this.doSearchRelated.bind(this)
    this.doOpenArticle = this.doOpenArticle.bind(this)
  }

  getClassnames () {
    return classNames([
      'article-card',
      {
        active: this.props.isActive
      }
    ])
  }

  doSelectArticle () {
    this.props.doSelectArticle(this.props.article.uid)
  }

  doSearchRelated () {
    if (this.state.isRelatedOpen) {
      this.setState({ isRelatedOpen: false })
    } else {
      this.setState({ isFetchingRelated: true }, () => {
        this.props.dispatch(articleRelatedLoad(this.props.article.uid)).then(() => {
          this.setState({ isFetchingRelated: false, isRelatedOpen: true })
        })
      })
    }
  }

  doOpenArticle () {
    this.props.history.push(ROUTES_DICT.ARTICLE_READ.replace(':uid', this.props.article.uid))
  }

  renderBody () {
    if (!this.props.isActive) {
      return null
    }
    return <React.Fragment>
      <div className='description'>{this.props.article.description}</div>
      {this.renderManage()}
      {this.renderRelated()}
    </React.Fragment>
  }

  renderRelated () {
    let related
    if (!this.state.isRelatedOpen) {
      return null
    }
    if (this.props.store.related.length > 0) {
      related = this.props.store.related.map((article) => {
        return <div key={article.uid} className='article'>
          <div className='title' title={article.title}>{article.title}</div>
          <div className='similar' title={article.similar}><Spinner size={18} value={article.similar} /></div>
        </div>
      })
    } else {
      related = <div className='empty'>Did not found relation articles</div>
    }
    return <div className='related-articles'>{related}</div>
  }

  renderManage () {
    const { isFetchingRelated, isRelatedOpen } = this.state
    return <div className='manage'>
      {/* <div className='left'></div> */}
      <div className='right'>
        <ButtonGroup>
          <Button icon='geosearch' onClick={this.doSearchRelated} loading={isFetchingRelated} active={isRelatedOpen} small>Similar</Button>
          <Button onClick={this.doOpenArticle} small>Open</Button>
        </ButtonGroup>
      </div>
    </div>
  }

  render () {
    const { title, datePublish, source } = this.props.article
    return <div className={this.getClassnames()} onClick={this.doSelectArticle}>
      <div className='header'>
        <div className='title' title={title}>{title}</div>
      </div>
      <div className='sub-header'>
        <div className='source'>by <a href={source.link}>{source.name.domain}</a></div>
        <div className='date-publish'>in {datePublish.format('YYYY-MM-DD HH:mm')}</div>
      </div>
      {this.renderBody()}
    </div>
  }
}

ArticleCard.proptTypes = {
  article: PropTypes.object.isRequired,
  isActive: PropTypes.bool.isRequired,
  doSelectArticle: PropTypes.func.isRequired
}

export default connect((state, props) => {
  const related = state.articles.related[props.article.uid]
  return {
    store: {
      related
    }
  }
})(withRouter(ArticleCard))
