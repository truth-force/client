import React from 'react'
import PropTypes from 'prop-types'
import {
  RangeSlider,
  ButtonGroup,
  Button
} from '@blueprintjs/core'

import './style.scss'

class ParagraphEditor extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      start: null,
      end: null,
      text: null,
      nodes: null,
      isApply: false
    }

    this.doChangeSelection = this.doChangeSelection.bind(this)
    this.doApply = this.doApply.bind(this)
  }

  componentDidMount () {
    const { text } = this.props
    const nodes = this.getNodes(text)
    this.setState({
      text,
      nodes,
      before: null,
      selection: text,
      after: null,
      start: 0,
      end: nodes.length - 1
    })
  }

  getNodes (text) {
    const re = new RegExp(' ', 'gi')
    let nodes = [0]
    while (re.exec(text)) {
      nodes.push(re.lastIndex)
    }
    nodes.push(text.length)
    return nodes
  }

  doChangeSelection ([start, end]) {
    const { nodes, text } = this.state
    const before = text.slice(0, nodes[start])
    const selection = text.slice(nodes[start], nodes[end] - 1)
    const after = text.slice(nodes[end] - 1)
    this.setState({ start, end, before, selection, after })
  }

  doApply () {
    this.setState({ isApply: true }, () => {
      this.props.doApply({
        selection: this.state.selection,
        start: this.state.start,
        end: this.state.end
      }).then(this.props.doCancel)
    })
  }

  renderSpans () {
    const { before, selection, after } = this.state
    return <React.Fragment>
      <span className='paragraph-selection'>{before}</span>
      <span className='paragraph-selection active'>{selection}</span>
      <span className='paragraph-selection'>{after}</span>
    </React.Fragment>
  }

  renderSlider () {
    const { nodes, start, end, isApply } = this.state
    const applyDisabled = start === end
    return <div className='paragraph-manage'>
      <RangeSlider
        min={0}
        max={nodes.length - 1}
        value={[start, end]}
        labelRenderer={false}
        className='paragraph-slider'
        onChange={this.doChangeSelection}
        disabled={this.state.isApply}
      />
      <ButtonGroup>
        <Button onClick={this.props.doCancel} intent='danger' disabled={isApply}>Cancel</Button>
        <Button onClick={this.doApply} intent='primary' loading={isApply} disabled={applyDisabled}>Apply</Button>
      </ButtonGroup>
    </div>
  }

  render () {
    if (!this.state.text) {
      return null
    }
    return <React.Fragment>
      {this.renderSpans()}
      {this.renderSlider()}
    </React.Fragment>
  }
}

ParagraphEditor.propTypes = {
  text: PropTypes.string.isRequired,
  doCancel: PropTypes.func.isRequired,
  doApply: PropTypes.func.isRequired
}

export default ParagraphEditor
