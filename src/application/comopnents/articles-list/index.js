import React from 'react'
import { connect } from 'react-redux'
import { Scrollbars } from 'react-custom-scrollbars'
import {
  Spinner,
  Button
} from '@blueprintjs/core'

import ArticleCard from '../article-card'

import { articlesListLoad, articlesListLoadMore } from '../../../store/actions/articles'

import './style.scss'

class ArticlesList extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      selectedUid: null,
      offset: 0,
      endOflist: false
    }

    this.doSelectArticle = this.doSelectArticle.bind(this)
    this.doLoadMore = this.doLoadMore.bind(this)
  }

  componentDidMount () {
    this.props.dispatch(articlesListLoad(true))
  }

  doLoadMore () {
    this.props.dispatch(articlesListLoadMore(this.props.store.articlesList.length)).then((data) => {
      if (data.articles.length === 0) {
        this.setState({ endOflist: true })
      }
    })
  }

  doSelectArticle (selectedUid) {
    this.setState({ selectedUid })
  }

  renderStatus () {
    let message = 'No data has been loaded yet'
    if (this.props.store.lastUpdate) {
      message = `Last update in ${this.props.store.lastUpdate.format('HH:mm:ss')}`
    } else if (this.props.store.isFetchingList) {
      message = <Spinner size={15} />
    }
    return <div className='articles-list_status'>
      <div className='last-update'>{message}</div>
    </div>
  }

  renderArticle (article) {
    return <ArticleCard
      key={article.uid}
      article={article}
      isActive={article.uid === this.state.selectedUid}
      doSelectArticle={this.doSelectArticle}
    />
  }

  renderLoadMoreButton () {
    if (this.state.endOflist) {
      return null
    }
    return <div className='articles-list_load-more'>
      <Button onClick={this.doLoadMore} loading={this.props.store.isFetchingList}>Load more</Button>
    </div>
  }

  renderArticlesList () {
    const articles = this.props.store.articlesList.map((article) => this.renderArticle(article))
    if (articles.length > 0) {
      return <Scrollbars autoHide>
        <div className='articles-list_elements'>{articles}</div>
        {this.renderLoadMoreButton()}
      </Scrollbars>
    } else if (this.props.store.isFetchingList) {
      return null
    } else {
      return <div className='articles-list_empty'>Empty list</div>
    }
  }

  render () {
    return <React.Fragment>
      {this.renderStatus()}
      {this.renderArticlesList()}
    </React.Fragment>
  }
}

export default connect((state) => {
  const { list, isFetchingList, lastUpdate } = state.articles
  return {
    store: { articlesList: list, isFetchingList, lastUpdate }
  }
})(ArticlesList)
