import React from 'react'
import classNames from 'classnames'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { Scrollbars } from 'react-custom-scrollbars'

import { articleCurrentLoad, articleStatementCreate } from '../../../store/actions/articles'
import ArticleParagraphs from '../article-paragraphs'

import './style.scss'

class ArticlePage extends React.PureComponent {
  constructor (props) {
    super(props)

    this.doApplyStatement = this.doApplyStatement.bind(this)
  }

  componentDidMount () {
    this.props.dispatch(articleCurrentLoad(this.props.match.params.uid))
  }

  componentDidUpdate (props) {
    if (this.props.match.params.uid !== props.match.params.uid) {
      this.props.dispatch(articleCurrentLoad(this.props.match.params.uid))
    }
  }

  doApplyStatement (pos) {
    const { uid } = this.props.store.current
    return this.props.dispatch(articleStatementCreate(uid, pos))
  }

  renderPublishDate () {
    const publishedIn = this.props.store.current.datePublish.format('MMMM Do YYYY, HH:mm')
    return <div className='published_in'>{publishedIn}</div>
  }

  renderAuthors () {
    let authors
    if (this.props.store.current.authors.length === 0) {
      return null
    }
    authors = this.props.store.current.authors.map((person) => person.name).join(', ')
    return <div className='writen_by'>Writen by {authors}</div>
  }

  renderContent () {
    const { title, text } = this.props.store.current
    return <Scrollbars autoHide>
      <div className='content'>
        <div className='article-title'>{title}</div>
        <div className='article-details'>
          {this.renderPublishDate()}
          {this.renderAuthors()}
        </div>
        <div className='article-text'>
          <ArticleParagraphs text={text} doApplyStatement={this.doApplyStatement} />
        </div>
      </div>
    </Scrollbars>
  }

  renderPage () {
    if (!this.props.store.current && !this.props.store.isFetchingCurrent) {
      return <div className='not-found'>Article not found</div>
    } else if (!this.props.store.current) {
      return null
    } else {
      return <div className='content-wrapper'>{this.renderContent()}</div>
    }
  }

  render () {
    const classes = classNames(['article-page', {
      fetching: this.props.store.isFetchingCurrent }
    ])
    return <div className={classes}>
      {this.renderPage()}
    </div>
  }
}

export default connect((state) => {
  const { current, isFetchingCurrent } = state.articles
  return {
    store: {
      current,
      isFetchingCurrent
    }
  }
})(withRouter(ArticlePage))
