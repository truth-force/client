import React from 'react'

import { BrowserRouter, Switch, Route } from 'react-router-dom'

import InitialScene from './scenes/initial'
import LoginScene from './scenes/login'
import ArticlesScene from './scenes/articles'

const ROUTES_DICT = {
  INDEX: '/',
  LOGIN: '/login',
  SIGNUP: '/signup',
  ARTICLES: '/articles',
  ARTICLE_READ: '/articles/:uid'
}

const ROUTES_SCENES = {
  [ROUTES_DICT.INDEX]: {
    component: InitialScene,
    exact: true
  },
  [ROUTES_DICT.LOGIN]: {
    component: LoginScene
  },
  [ROUTES_DICT.SIGNUP]: {
    component: InitialScene
  },
  [ROUTES_DICT.ARTICLES]: {
    component: ArticlesScene
  }
}

const generateRoutes = () => (
  <BrowserRouter>
    <Switch>
      {Object.entries(ROUTES_SCENES).map(([path, props]) => {
        return <Route key={path} path={path} {...props} />
      })}
    </Switch>
  </BrowserRouter>
)

export default generateRoutes
export {
  ROUTES_DICT,
  generateRoutes
}
