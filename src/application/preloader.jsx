import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  Spinner
} from '@blueprintjs/core'

import { authConnect } from '../store/actions/auth'

const TRANSITION = 500

class Preloader extends React.PureComponent {
  constructor (props) {
    super(props)

    this.state = {
      ready: false,
      finished: false,
      error: null
    }

    this.doSetFinished = this.doSetFinished.bind(this)
  }

  getPreloaderStyles () {
    const { ready } = this.state
    return {
      transition: `all ${TRANSITION}ms`,
      opacity: ready ? 0 : 1,
      transform: `scale(${ready ? 0.9 : 1})`
    }
  }

  componentDidMount () {
    this.props.dispatch(authConnect()).then(() => {
      this.setState({ ready: true, error: null })
    }).catch((error) => {
      this.setState({ ready: true, error })
    }).then(this.props.onReady).then(this.doSetFinished)
  }

  doSetFinished (error = null) {
    setTimeout(() => {
      this.setState({ finished: true, error })
    }, TRANSITION)
  }

  render () {
    if (!this.state.finished) {
      return <div className='preloader' style={this.getPreloaderStyles()}><Spinner /></div>
    }
    return null
  }
}

Preloader.propTypes = {
  onReady: PropTypes.func.isRequired
}

export default connect()(Preloader)
