import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import { Logger } from './utils/logger'
import { startStore } from './store'
import AuthService from './services/auth'
import HttpService from './services/http'
import ToastService from './services/toast'
import Application from './application'

const log = Logger('web-application')

class WebApplication {
  constructor () {
    this.services = {}
    this.store = null
  }

  async startApplication () {
    return startStore(this.services).then((store) => {
      this.store = store
    }).then(() => {
      this.services.auth = new AuthService(this.store, this.services)
      this.services.http = new HttpService()
      this.services.toast = new ToastService()
    })
  }

  start (root) {
    return this.startApplication().then(() => {
      this.render(root)
    }).catch(log.error)
  }

  render (root) {
    const application = (
      <Provider store={this.store}>
        <Application services={this.services} />
      </Provider>
    )
    ReactDOM.render(application, root)
  }
}

const webApplication = new WebApplication()
const root = document.getElementById('root')

webApplication.start(root)
