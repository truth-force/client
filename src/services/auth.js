import * as jwt from 'jsonwebtoken'
import Cookie from 'js-cookie'

import COOKIES from '../../etc/cookies'
import ENDPOINTS from '../../etc/endpoints'

class AuthService {
  constructor (store, services) {
    this.store = store
    this.services = services
  }

  async parseToken (token) {
    return new Promise((resolve, reject) => {
      try {
        resolve(jwt.decode(token))
      } catch (error) {
        reject(error)
      }
    })
  }

  async applyToken (token, remember) {
    if (!token) {
      return Promise.reject(new Error('Invalid token'))
    }
    return this.parseToken(token).then((decode) => {
      const expires = remember && decode.exp ? new Date(decode.exp * 1000) : null
      this.services.http.addHeader('Authorization', `Bearer ${token}`)
      Cookie.set(COOKIES.TOKEN, token, { expires })
      return decode.payload
    })
  }

  async connect () {
    const token = Cookie.get(COOKIES.TOKEN)
    if (!token) {
      return null
    }
    return this.services.http.post(ENDPOINTS.CONNECT, { token }).then(({ code, data }) => {
      const { token, error } = data
      if (code !== 200 || error) {
        Cookie.remove(COOKIES.TOKEN)
        return Promise.reject(new Error(error))
      } else if (code === 203) {
        return null
      }
      return this.applyToken(token, true)
    })
  }

  async login (username, password, remember) {
    if (!username && !password) {
      return Promise.reject(new Error('Invalid username or password'))
    }
    return this.softLogout().then(() => {
      return this.services.http.post(ENDPOINTS.LOGIN, { username, password }, { delay: 1000 }).then(({ code, data }) => {
        const { token, error } = data
        if (code !== 200 && error) {
          return Promise.reject(new Error(error))
        }
        return this.applyToken(token, remember)
      })
    })
  }

  async logout () {
    const token = Cookie.get(COOKIES.TOKEN)
    if (token) {
      Cookie.remove(COOKIES.TOKEN)
      return this.services.http.post(ENDPOINTS.LOGOUT).then(({ code, data }) => {
        const { error } = data
        this.services.http.removeHeader('Authorization')
        if (code !== 200 && error) {
          return Promise.reject(new Error(error))
        }
      })
    }
  }

  async softLogout () {
    return new Promise((resolve, reject) => {
      try {
        if (Cookie.get(COOKIES.TOKEN)) {
          Cookie.remove(COOKIES.TOKEN)
        }
        return resolve()
      } catch (error) {
        return reject(error)
      }
    })
  }
}

export default AuthService
