/* global XMLHttpRequest */
import CONFIG from '../../etc/config'

class HttpService {
  constructor () {
    this.API_ROOT = CONFIG.API_ROOT
    this.headers = {}
  }

  addHeader (name, value) {
    this.headers[name] = value
  }

  removeHeader (name) {
    delete this.headers[name]
  }

  async sendJson (endpoint, payload = {}, method = 'GET', props = {}) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest()
      let params = ''
      if (props.params) {
        params = '?' + Object.entries(props.params).filter((item) => {
          return item[1] !== undefined
        }).map(([key, value]) => {
          return `${key}=${encodeURIComponent(value)}`
        }).join('&')
      }
      xhr.open(method, this.API_ROOT + endpoint + params, true)
      xhr.setRequestHeader('Content-Type', 'application/json')
      Object.entries(this.headers).forEach(([name, value]) => {
        xhr.setRequestHeader(name, value)
      })
      xhr.onloadend = () => { resolve(xhr) }
      xhr.onerror = () => { reject(new Error(xhr.statusText)) }
      setTimeout(() => {
        xhr.send(payload ? JSON.stringify(payload) : null)
      }, props.delay)
    }).then((xhr) => {
      return {
        code: xhr.status,
        data: xhr.responseText ? JSON.parse(xhr.responseText) : null
      }
    })
  }

  async get (endpoint, props) {
    return this.sendJson(endpoint, null, 'GET', props)
  }

  async post (endpoint, payload, props) {
    return this.sendJson(endpoint, payload, 'POST', props)
  }

  async put (endpoint, payload, props) {
    return this.sendJson(endpoint, payload, 'PUT', props)
  }

  async remove (endpoint, props) {
    return this.sendJson(endpoint, null, 'DELETE', props)
  }
}

export default HttpService
