import {
  Toaster
} from '@blueprintjs/core'

class ToastService {
  constructor () {
    this.Toaster = Toaster.create()
  }

  show (...props) {
    this.Toaster.show(...props)
  }

  success (message) {
    this.show({ message, intent: 'success' })
  }

  fail (message) {
    this.show({ message, intent: 'danger' })
  }
}

export default ToastService
