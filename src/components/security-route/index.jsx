import React from 'react'
import { connect } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'

function renderBuilder (Component, { field, eq, redirect, deny, store }) {
  let access = deny
  if (eq === undefined && store.user && store.user[field]) {
    access = !access
  } else if (eq !== undefined && store.user && store.user[field] === eq) {
    access = !access
  }
  return access ? (props) => <Component {...props} /> : () => <Redirect to={redirect} />
}

function SecurityRoute ({ component, field, eq, redirect = '/', deny = false, store, ...props }) {
  return <Route {...props} render={renderBuilder(component, { field, eq, redirect, deny, store })} />
}

export default connect((state) => {
  return {
    store: {
      user: state.auth.user
    }
  }
})(SecurityRoute)
