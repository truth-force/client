import * as TYPES from './types'

export function authConnect () {
  return (dispatch, getState, { services }) => {
    return services.auth.connect().then((user) => {
      return dispatch({ type: TYPES.AUTH_LOGIN_SUCCESS, user })
    })
  }
}

export function authLogin (username, password, remember) {
  return (dispatch, getState, { services }) => {
    dispatch({ type: TYPES.AUTH_LOGIN_FETCH })
    return services.auth.login(username, password, remember).then((user) => {
      services.toast.success('Congratulations! Are you in Game! 👍')
      return dispatch({ type: TYPES.AUTH_LOGIN_SUCCESS, user })
    }).catch((error) => {
      services.toast.fail(`So sorry 😔`)
      return dispatch({ type: TYPES.AUTH_LOGIN_FAIL, error })
    })
  }
}

export function authLogout () {
  return (dispatch, getState, { services }) => {
    return services.auth.logout().then(() => {
      return dispatch({ type: TYPES.AUTH_LOGOUT })
    })
  }
}
