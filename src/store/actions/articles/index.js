import moment from 'moment'
import parseDomain from 'parse-domain'

import ENDPOINTS from '../../../../etc/endpoints'
import * as TYPES from './types'

export function articlesListLoad (initiate = false, offset, limit = 23) {
  return (dispatch, getState, { services }) => {
    dispatch({ type: TYPES.ARTICLES_LIST_FETCH })
    return services.http.get(ENDPOINTS.ARTICLES.ROOT, { params: { limit, offset } }).then(({ code, data }) => {
      let articles = []
      if (code === 200) {
        articles = data.map((article) => {
          article.datePublish = moment(article.datePublish)
          article.source = { link: article.sourceUrl, name: parseDomain(article.sourceUrl) }
          return article
        })
      }
      return dispatch({ type: TYPES.ARTICLES_LIST_SUCCESS, articles, initiate })
    })
  }
}

export function articlesListLoadMore (offset) {
  return articlesListLoad(false, offset)
}

export function articleRelatedLoad (uid) {
  return (dispatch, getState, { services }) => {
    dispatch({ type: TYPES.ARTICLES_RELATED_FETCH })
    return services.http.get(ENDPOINTS.ARTICLES.RELATED.replace(':uid', uid)).then(({ code, data }) => {
      let related = []
      if (code === 200) {
        related = data.map((article) => {
          article.source = { link: article.sourceUrl, name: parseDomain(article.source_url) }
          return article
        })
      }
      return dispatch({ type: TYPES.ARTICLES_RELATED_SUCCESS, related, uid })
    })
  }
}

export function articleCurrentLoad (uid) {
  return (dispatch, getState, { services }) => {
    dispatch({ type: TYPES.ARTICLES_CURRENT_FETCH })
    return services.http.get(ENDPOINTS.ARTICLES.BY_UID.replace(':uid', uid)).then(({ code, data }) => {
      let article = data
      if (code === 200) {
        article.datePublish = moment(article.datePublish)
        article.source = { link: article.sourceUrl, name: parseDomain(article.sourceUrl) }
      }
      return dispatch(articleStatementsLoad(uid)).then(() => {
        return dispatch({ type: TYPES.ARTICLES_CURRENT_SUCCESS, article })
      })
    })
  }
}

export function articleStatementCreate (articleUid, pos) {
  return (dispatch, getState, { services }) => {
    return services.http.post(ENDPOINTS.STATEMENTS.ROOT, { articleUid, pos }).then(({ code, data }) => {
      if (code === 201) {
        services.toast.success('Statement added!')
        return dispatch(articleStatementsLoad(articleUid))
      } else {
        services.toast.fail(`Unexpected error. Try later`)
        throw new Error(`error code: ${code}`)
      }
    })
  }
}

export function articleStatementsLoad (uid) {
  return (dispatch, getState, { services }) => {
    dispatch({ type: TYPES.ARTICLES_STATEMENTS_FETCH })
    return services.http.get(ENDPOINTS.ARTICLES.STATEMENTS.replace(':uid', uid)).then(({ code, data }) => {
      let statements = data.reduce((res, statement) => {
        if (res[statement.articleUid]) {
          res[statement.articleUid].push(statement)
        } else {
          res[statement.articleUid] = [statement]
        }
        return res
      }, {})
      return dispatch({ type: TYPES.ARTICLES_STATEMENTS_SUCCESS, statements })
    })
  }
}
