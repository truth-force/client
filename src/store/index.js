import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import reducers from './reducers'

export const startStore = async (services) => {
  return createStore(
    combineReducers(reducers),
    applyMiddleware(thunk.withExtraArgument({ services }))
  )
}
