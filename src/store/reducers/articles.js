import moment from 'moment'

import * as TYPES from '../actions/articles/types'

const INITIAL_STATE = {
  isFetchingList: false,
  isFetchingCurrent: false,
  isFetchingRelated: false,
  isFetchingStatements: false,
  list: [],
  statements: [],
  related: {},
  current: null,
  lastUpdate: null
}

export function articles (state = INITIAL_STATE, { type, ...data }) {
  switch (type) {
    case TYPES.ARTICLES_LIST_FETCH:
      return {
        ...state,
        isFetchingList: true
      }
    case TYPES.ARTICLES_LIST_SUCCESS:
      let articles
      if (data.initiate) {
        articles = data.articles
      } else {
        articles = [...state.list, ...data.articles]
      }
      return {
        ...state,
        list: articles,
        isFetchingList: false,
        lastUpdate: moment()
      }
    case TYPES.ARTICLES_RELATED_FETCH:
      return {
        ...state,
        isFetchingRelated: true
      }
    case TYPES.ARTICLES_RELATED_SUCCESS:
      let { related } = state
      related[data.uid] = data.related
      return {
        ...state,
        isFetchingRelated: false,
        related
      }
    case TYPES.ARTICLES_CURRENT_FETCH:
      return {
        ...state,
        current: null,
        isFetchingCurrent: true
      }
    case TYPES.ARTICLES_CURRENT_SUCCESS:
      return {
        ...state,
        current: data.article,
        isFetchingCurrent: false
      }
    case TYPES.ARTICLES_STATEMENTS_FETCH:
      return {
        ...state,
        statements: [],
        isFetchingStatements: true
      }
    case TYPES.ARTICLES_STATEMENTS_SUCCESS:
      return {
        ...state,
        statements: data.statements,
        isFetchingStatements: false
      }
    default:
      return state
  }
}
