import * as TYPES from '../actions/auth/types'

const INITIAL_STATE = {
  isFetching: false,
  isLoggedIn: false,
  user: null,
  error: null
}

export function auth (state = INITIAL_STATE, { type, ...data }) {
  switch (type) {
    case TYPES.AUTH_LOGIN_FAIL:
      return {
        ...INITIAL_STATE,
        error: data
      }
    case TYPES.AUTH_LOGIN_FETCH:
      return {
        ...INITIAL_STATE,
        isFetching: true
      }
    case TYPES.AUTH_LOGIN_SUCCESS:
      return {
        ...INITIAL_STATE,
        isLoggedIn: !!data,
        user: data
      }
    case TYPES.AUTH_LOGOUT:
      return INITIAL_STATE
    default:
      return state
  }
}
