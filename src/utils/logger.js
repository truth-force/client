import loglevel from 'loglevel'
import * as prefix from 'loglevel-plugin-prefix'

prefix.reg(loglevel)
prefix.apply(loglevel, {
  template: '[%t] %n %l:',
  nameFormatter: (name) => name ? name.toUpperCase() : 'ROOT'
})

const Logger = loglevel.getLogger
const setLogLevel = loglevel.setLevel
const levels = loglevel.levels

export default Logger
export {
  Logger,
  setLogLevel,
  levels
}
