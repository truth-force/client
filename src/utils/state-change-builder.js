export default function (self, extract = () => { /* Empty function */ }) {
  return (prop) => {
    return (value) => {
      self.setState({ [prop]: extract(value) })
    }
  }
}
